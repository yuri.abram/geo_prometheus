package component

import (
	"github.com/ptflp/godecoder"
	"gitlab/geo_prometheus/config"
	"gitlab/geo_prometheus/internal/infrastructure/metrics"
	"gitlab/geo_prometheus/internal/infrastructure/responder"
	"go.uber.org/zap"
)

type Components struct {
	Conf      config.AppConf
	Responder responder.Responder
	Logger    *zap.Logger
	Decoder   godecoder.Decoder
	Metrics   metrics.Metrics
}

func NewComponents(conf config.AppConf, responder responder.Responder, logger *zap.Logger, decoder godecoder.Decoder, metrics metrics.Metrics) *Components {
	return &Components{Conf: conf, Responder: responder, Logger: logger, Decoder: decoder, Metrics: metrics}
}
