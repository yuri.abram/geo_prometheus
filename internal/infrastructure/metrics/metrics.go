package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"time"
)

type Metrics interface {
	CounterInc(name string)
	DurationSummary(name string, duration time.Duration)
}

type PrometheusMetrics struct {
	registry       *prometheus.Registry
	requestMetrics map[string]struct {
		countMetric    prometheus.Counter
		durationMetric prometheus.Summary
	}
}

func NewPrometheusMetrics(registry *prometheus.Registry) *PrometheusMetrics {
	return &PrometheusMetrics{
		registry: registry,
		requestMetrics: make(map[string]struct {
			countMetric    prometheus.Counter
			durationMetric prometheus.Summary
		}),
	}
}

// CounterInc увеличивает значение метрики
func (pm *PrometheusMetrics) CounterInc(name string) {
	pm.createMetricsForMethodName(name)
	pm.requestMetrics[name].countMetric.Inc()
}

// DurationSummary записывает значение метрики summary
func (pm *PrometheusMetrics) DurationSummary(name string, duration time.Duration) {
	pm.createMetricsForMethodName(name)
	pm.requestMetrics[name].durationMetric.Observe(float64(duration))
}

// createCountMetric Создает метрику для подсчета количества вызовов запроса
func (pm *PrometheusMetrics) createCountMetric(name string, labels map[string]string) prometheus.Counter {
	return promauto.NewCounter(
		prometheus.CounterOpts{
			Name:        name,
			Help:        "Count of " + name,
			ConstLabels: labels,
		})
}

// createDurationMetric Создает метрику для измерения времени выполнения запроса
func (pm *PrometheusMetrics) createDurationMetric(name string, labels map[string]string) prometheus.Summary {
	summaryOpts := prometheus.SummaryOpts{
		Name:        name,
		Help:        "Summary of " + name + " duration",
		ConstLabels: labels,
	}

	return promauto.NewSummary(summaryOpts)
}

func (pm *PrometheusMetrics) createMetricsForMethodName(methodName string) {
	if _, ok := pm.requestMetrics[methodName]; !ok {
		pm.requestMetrics[methodName] = struct {
			countMetric    prometheus.Counter
			durationMetric prometheus.Summary
		}{
			countMetric:    pm.createCountMetric(methodName+"_requests_total", map[string]string{}),
			durationMetric: pm.createDurationMetric(methodName+"_request_duration", map[string]string{}),
		}
	}
}
