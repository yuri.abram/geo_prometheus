package models

type SearchHistory struct {
	ID    int    `json:"id" db:"id" db_type:"SERIAL primary key" db_default:"not null"`
	Query string `json:"query" db:"query" db_type:"varchar(255)" db_ops:"create,update"`
}

func (s *SearchHistory) TableName() string {
	return "search_history"
}

func (s *SearchHistory) OnCreate() []string {
	return []string{}
}

type AddressData struct {
	ID        int      `json:"id" db:"id" db_type:"SERIAL primary key"`
	Addresses []string `json:"addresses" db:"addresses" db_type:"varchar" db_ops:"create,update"`
}

func (a *AddressData) OnCreate() []string {
	return []string{}
}

func (a *AddressData) TableName() string {
	return "address_data"
}

type SearchAddressLink struct {
	QueryID     int `json:"query_id" db:"query_id" db_type:"INTEGER" db_ops:"create,update"`
	AddressesID int `json:"addresses_id" db:"addresses_id" db_type:"INTEGER, PRIMARY KEY (query_id, addresses_id)" db_ops:"create,update"`
}

func (s *SearchAddressLink) TableName() string {
	return "search_address_link"
}

func (s *SearchAddressLink) OnCreate() []string {
	return []string{}
}
