package controller

import (
	"encoding/json"
	"gitlab/geo_prometheus/internal/infrastructure/component"
	"gitlab/geo_prometheus/internal/infrastructure/metrics"
	"gitlab/geo_prometheus/internal/infrastructure/responder"
	"gitlab/geo_prometheus/internal/models"
	"gitlab/geo_prometheus/internal/modules/geo/service"
	"net/http"
	"time"
)

type GeoController interface {
	SearchResponding(w http.ResponseWriter, r *http.Request)
}

type Geo struct {
	responder responder.Responder
	service   service.GeoServicer
	metrics   metrics.Metrics
}

func NewGeoCtl(service service.GeoServicer, components *component.Components) *Geo {
	return &Geo{responder: components.Responder, service: service, metrics: components.Metrics}
}

// SearchResponding
// @Summary 		Поиск адреса
// @Description Поиск адреса./
// @ID 			search
// @Tags 		geocode
// @Accept 		json
// @Produce 	json
// @Param 		request body 		models.SearchRequest true "Search request"
// @Success 	200 	{object} 	models.GeoResponse
// @Failure 	400		{string}	string "Bad request"
// @Failure 	500		{string}	string "Internal server error"
// @Router		/api/address/search [post]
func (g *Geo) SearchResponding(w http.ResponseWriter, r *http.Request) {

	// Метрики //
	metricName := "SearchResponding"
	g.metrics.CounterInc(metricName) // создаст новую если не существует
	start := time.Now()
	defer func() {
		g.metrics.DurationSummary(metricName, time.Since(start)) // создаст новую если не существует
	}()
	//

	var sr models.SearchRequest
	err := json.NewDecoder(r.Body).Decode(&sr)
	if err != nil {
		g.responder.ErrorBadRequest(w, err)
		return
	}
	query := sr.Query
	out, err := g.service.Search(r.Context(), query)
	if err != nil {
		g.responder.ErrorBadRequest(w, err)
	}
	g.responder.OutputJSON(w, out)
}
