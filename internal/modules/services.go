package modules

import (
	"gitlab/geo_prometheus/internal/infrastructure/component"
	aservice "gitlab/geo_prometheus/internal/modules/auth/service"
	gserv "gitlab/geo_prometheus/internal/modules/geo/service"
)

type Services struct {
	GeoService gserv.GeoServicer
	Auth       aservice.Auther
}

func NewServices(storage *Storages, components *component.Components) *Services {
	return &Services{
		GeoService: gserv.NewGeoService(components.Logger, storage.Geo),
		Auth:       aservice.NewAuth(),
	}
}
