package modules

import (
	"gitlab/geo_prometheus/internal/db/adapter"
	gstorage "gitlab/geo_prometheus/internal/modules/geo/storage"
)

type Storages struct {
	Geo gstorage.GeoStorager
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		Geo: gstorage.NewGeoStorage(sqlAdapter),
	}
}
